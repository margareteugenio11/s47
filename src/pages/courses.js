import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'
import {Fragment} from 'react'

export default function Courses(){
	console.log(coursesData)
	console.log(coursesData[0])

const courses = coursesData.map(course => {
	return (
		<CourseCard key = {course.id} courseProp = {course} />
	)
})
	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	)
}
