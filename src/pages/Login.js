import {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Fragment} from 'react'

export default function Register() {

	// State hooks to store the values of the input fields.
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	// State to determine whether register button is enabled or not.
	const [isActive, setIsActive] = useState(false)

	function loginUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		//Clears the input fields
		setEmail('')
		setPassword('')

		alert('You are now logged in!');
	}

	useEffect(() => {
		if((email !== '') && (password !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return (
		<Fragment>
		<h1>Login</h1>
		<Form onSubmit={(e)=> loginUser(e)}>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type= 'email'	
					placeholder= 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className = "text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please input your password here'
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'> 
					Login 
				</Button> :
				<Button variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
					Login
				</Button>
			}
		</Form>
		</Fragment>
	)
}
